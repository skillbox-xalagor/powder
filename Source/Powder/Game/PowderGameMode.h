// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PowderGameMode.generated.h"

UCLASS(minimalapi)
class APowderGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APowderGameMode();

	void PlayerCharacterDead();
};
