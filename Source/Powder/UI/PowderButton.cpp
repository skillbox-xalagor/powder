#include "PowderButton.h"

#include "CommonTextBlock.h"


void UPowderButton::NativeOnCurrentTextStyleChanged()
{
	Super::NativeOnCurrentTextStyleChanged();
	MyButtonTextLabel->SetStyle(GetCurrentTextStyleClass());
}

void UPowderButton::SetPowderText(const FText& InText)
{
	MyButtonTextLabel->SetText(InText);
}