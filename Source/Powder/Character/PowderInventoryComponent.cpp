// Fill out your copyright notice in the Description page of Project Settings.

#include "PowderInventoryComponent.h"

#include "Net/UnrealNetwork.h"
#include "Powder/Game/PowderGameInstance.h"
#include "Powder/Interface/PowderInteraction.h"

int32 DebugInventoryShow = 0;
FAutoConsoleVariableRef CVarInventoryShow(
	TEXT("Powder.DebugInventory"),
	DebugInventoryShow,
	TEXT("Draw Debug for Inventory"),
	ECVF_Cheat);

// Sets default values for this component's properties
UPowderInventoryComponent::UPowderInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicatedByDefault(true);
	// ...
}

// Called when the game starts
void UPowderInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}

// Called every frame
void UPowderInventoryComponent::TickComponent(
	const float DeltaTime, const ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!DebugInventoryShow) return;
	if (GetOwner()->GetLocalRole() == ROLE_Authority)
	{
		GEngine->AddOnScreenDebugMessage(420, 0.1f, FColor::Yellow, TEXT("Weapons"));
		for (int i = 0; i < WeaponSlots.Max(); ++i)
		{
			GEngine->AddOnScreenDebugMessage(i, 0.1f, FColor::Silver,
				FString::Printf(
					TEXT("WeaponSlot[%s]: %d"), *WeaponSlots[i].NameItem.ToString(), WeaponSlots[i].AdditionalInfo.Round));
		}
	}
}

bool UPowderInventoryComponent::SwitchWeaponToIndexByNextPrevIndex(
	int32 ChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo, bool bIsForward)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlots.Num() - 1)
	{
		CorrectIndex = 0;
	}
	else if (ChangeToIndex < 0)
	{
		CorrectIndex = WeaponSlots.Num() - 1;
	}

	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;
	int32 NewCurrentIndex = 0;

	if (WeaponSlots.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
		{
			if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
			{
				// good weapon have ammo start change
				bIsSuccess = true;
			}
			else
			{
				UPowderGameInstance* MyGameInstance = Cast<UPowderGameInstance>(GetWorld()->GetGameInstance());
				if (MyGameInstance)
				{
					// check ammoSlots for this weapon
					FWeaponInfo MyInfo;
					MyGameInstance->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, MyInfo);

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind)
					{
						if (AmmoSlots[j].WeaponType == MyInfo.WeaponType && AmmoSlots[j].Cout > 0)
						{
							// good weapon have ammo start change
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
			}
		}
	}
	if (!bIsSuccess)
	{
		int8 Iteration = 0;
		int8 SecondIteration = 0;
		int8 TmpIndex;
		while (Iteration < WeaponSlots.Num() && !bIsSuccess)
		{
			Iteration++;

			if (bIsForward)
			{
				// Seconditeration = 0;

				TmpIndex = ChangeToIndex + Iteration;
			}
			else
			{
				SecondIteration = WeaponSlots.Num() - 1;

				TmpIndex = ChangeToIndex - Iteration;
			}

			if (WeaponSlots.IsValidIndex(TmpIndex))
			{
				if (!WeaponSlots[TmpIndex].NameItem.IsNone())
				{
					if (WeaponSlots[TmpIndex].AdditionalInfo.Round > 0)
					{
						// WeaponGood
						bIsSuccess = true;
						NewIdWeapon = WeaponSlots[TmpIndex].NameItem;
						NewAdditionalInfo = WeaponSlots[TmpIndex].AdditionalInfo;
						NewCurrentIndex = TmpIndex;
					}
					else
					{
						FWeaponInfo myInfo;
						UPowderGameInstance* myGI = Cast<UPowderGameInstance>(GetWorld()->GetGameInstance());

						myGI->GetWeaponInfoByName(WeaponSlots[TmpIndex].NameItem, myInfo);

						bool bIsFind = false;
						int8 j = 0;
						while (j < AmmoSlots.Num() && !bIsFind)
						{
							if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
							{
								// WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[TmpIndex].NameItem;
								NewAdditionalInfo = WeaponSlots[TmpIndex].AdditionalInfo;
								NewCurrentIndex = TmpIndex;
								bIsFind = true;
							}
							j++;
						}
					}
				}
			}
			else
			{
				// go to end of LEFT of array weapon slots
				if (OldIndex != SecondIteration)
				{
					if (WeaponSlots.IsValidIndex(SecondIteration))
					{
						if (!WeaponSlots[SecondIteration].NameItem.IsNone())
						{
							if (WeaponSlots[SecondIteration].AdditionalInfo.Round > 0)
							{
								// WeaponGood
								bIsSuccess = true;
								NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
								NewAdditionalInfo = WeaponSlots[SecondIteration].AdditionalInfo;
								NewCurrentIndex = SecondIteration;
							}
							else
							{
								FWeaponInfo myInfo;
								UPowderGameInstance* myGI = Cast<UPowderGameInstance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, myInfo);

								bool bIsFind = false;
								int8 j = 0;
								while (j < AmmoSlots.Num() && !bIsFind)
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
									{
										// WeaponGood
										bIsSuccess = true;
										NewIdWeapon = WeaponSlots[SecondIteration].NameItem;
										NewAdditionalInfo = WeaponSlots[SecondIteration].AdditionalInfo;
										NewCurrentIndex = SecondIteration;
										bIsFind = true;
									}
									j++;
								}
							}
						}
					}
				}
				else
				{
					// go to same weapon when start
					if (WeaponSlots.IsValidIndex(SecondIteration))
					{
						if (!WeaponSlots[SecondIteration].NameItem.IsNone())
						{
							if (WeaponSlots[SecondIteration].AdditionalInfo.Round > 0)
							{
								// WeaponGood, it same weapon do nothing
							}
							else
							{
								FWeaponInfo myInfo;
								UPowderGameInstance* myGI = Cast<UPowderGameInstance>(GetWorld()->GetGameInstance());

								myGI->GetWeaponInfoByName(WeaponSlots[SecondIteration].NameItem, myInfo);

								int8 j = 0;
								while (j < AmmoSlots.Num())
								{
									if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
									{
										if (AmmoSlots[j].Cout > 0)
										{
											// WeaponGood, it same weapon do nothing
										}
										else
										{
											// Not find weapon with ammo need init Pistol with infinity ammo
											UE_LOG(LogTemp, Error,
												TEXT("UPowderInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
										}
									}
									j++;
								}
							}
						}
					}
				}
				if (bIsForward)
				{
					SecondIteration++;
				}
				else
				{
					SecondIteration--;
				}
			}
		}
	}
	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);

		// OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
		SwitchWeaponEvent_OnServer(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
	}

	return bIsSuccess;
}

bool UPowderInventoryComponent::SwitchWeaponByIndex(
	int32 IndexWeaponToChange, int32 PreviousIndex, FAdditionalWeaponInfo PreviousWeaponInfo)
{
	bool bIsSuccess = false;

	const FName ToSwitchIdWeapon = GetWeaponNameBySlotIndex(IndexWeaponToChange);
	const FAdditionalWeaponInfo ToSwitchAdditionalInfo = GetAdditionalInfoWeapon(IndexWeaponToChange);

	if (!ToSwitchIdWeapon.IsNone())
	{
		SetAdditionalInfoWeapon(PreviousIndex, PreviousWeaponInfo);

		// OnSwitchWeapon.Broadcast(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);
		SwitchWeaponEvent_OnServer(ToSwitchIdWeapon, ToSwitchAdditionalInfo, IndexWeaponToChange);

		// check ammo slot for event to player
		EWeaponType ToSwitchWeaponType;
		if (GetWeaponTypeByNameWeapon(ToSwitchIdWeapon, ToSwitchWeaponType))
		{
			int8 AvailableAmmoForWeapon = -1;
			if (CheckAmmoForWeapon(ToSwitchWeaponType, AvailableAmmoForWeapon))
			{
			}
		}
		bIsSuccess = true;
	}
	return bIsSuccess;
}

FAdditionalWeaponInfo UPowderInventoryComponent::GetAdditionalInfoWeapon(const int32 IndexWeapon)
{
	FAdditionalWeaponInfo Result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				Result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UPowderInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"),
				IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UPowderInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"),
			IndexWeapon);

	return Result;
}

int32 UPowderInventoryComponent::GetWeaponIndexSlotByName(const FName IdWeaponName)
{
	int32 Result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			Result = i;
		}
		i++;
	}
	return Result;
}

FName UPowderInventoryComponent::GetWeaponNameBySlotIndex(const int32 IndexSlot)
{
	FName Result;

	if (WeaponSlots.IsValidIndex(IndexSlot))
	{
		Result = WeaponSlots[IndexSlot].NameItem;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UPowderInventoryComponent::GetWeaponNameBySlotIndex - Not Correct index Weapon  - %d"),
			IndexSlot);
	}
	return Result;
}

bool UPowderInventoryComponent::GetWeaponTypeByIndexSlot(const int32 IndexSlot, EWeaponType& WeaponType)
{
	bool bIsFind = false;
	WeaponType = EWeaponType::RifleType;
	UPowderGameInstance* MyGameInstance = Cast<UPowderGameInstance>(GetWorld()->GetGameInstance());
	if (MyGameInstance)
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			FWeaponInfo OutInfo;
			MyGameInstance->GetWeaponInfoByName(WeaponSlots[IndexSlot].NameItem, OutInfo);
			WeaponType = OutInfo.WeaponType;
			bIsFind = true;
		}
	}
	return bIsFind;
}

bool UPowderInventoryComponent::GetWeaponTypeByNameWeapon(const FName IdWeaponName, EWeaponType& WeaponType) const
{
	bool bIsFind = false;
	WeaponType = EWeaponType::RifleType;
	UPowderGameInstance* MyGameInstance = Cast<UPowderGameInstance>(GetWorld()->GetGameInstance());
	if (MyGameInstance)
	{
		FWeaponInfo OutInfo;
		MyGameInstance->GetWeaponInfoByName(IdWeaponName, OutInfo);
		WeaponType = OutInfo.WeaponType;
		bIsFind = true;
	}
	return bIsFind;
}

void UPowderInventoryComponent::SetAdditionalInfoWeapon(const int32 IndexWeapon, const FAdditionalWeaponInfo NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlots[i].AdditionalInfo = NewInfo;
				bIsFind = true;

				// OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon,NewInfo);
				WeaponAdditionalInfoChangeEvent_Multicast(IndexWeapon, NewInfo);
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UPowderInventoryComponent::SetAdditionalInfoWeapon - Not Found Weapon with index - %d"),
				IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UPowderInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"),
			IndexWeapon);
}

void UPowderInventoryComponent::AmmoSlotChangeValue(const EWeaponType TypeWeapon, const int32 CountChangeAmmo)
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Cout += CountChangeAmmo;
			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
			{
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;
			}

			AmmoChangeEvent_Multicast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);
			// OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout);

			bIsFind = true;
		}
		i++;
	}
}

bool UPowderInventoryComponent::CheckAmmoForWeapon(const EWeaponType TypeWeapon, int8& AvailableAmmoForWeapon)
{
	AvailableAmmoForWeapon = 0;
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			bIsFind = true;
			AvailableAmmoForWeapon = AmmoSlots[i].Cout;
			if (AmmoSlots[i].Cout > 0)
			{
				return true;
			}
		}
		i++;
	}

	if (AvailableAmmoForWeapon <= 0)
	{
		// OnWeaponAmmoEmpty.Broadcast(TypeWeapon);
		WeaponAmmoEmptyEvent_Multicast(TypeWeapon);
	}
	else
	{
		// OnWeaponAmmoAvailable.Broadcast(TypeWeapon);
		WeaponAmmoAvailableEvent_Multicast(TypeWeapon);
	}

	return false;
}

bool UPowderInventoryComponent::CheckCanTakeAmmo(const EWeaponType AmmoType)
{
	bool bResult = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bResult)
	{
		if (AmmoSlots[i].WeaponType == AmmoType && AmmoSlots[i].Cout < AmmoSlots[i].MaxCout)
		{
			bResult = true;
		}
		i++;
	}
	return bResult;
}

bool UPowderInventoryComponent::CheckCanTakeWeapon(int32& FreeSlot)
{
	bool bIsFreeSlot = false;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsFreeSlot)
	{
		if (WeaponSlots[i].NameItem.IsNone())
		{
			bIsFreeSlot = true;
			FreeSlot = i;
		}
		i++;
	}
	return bIsFreeSlot;
}

bool UPowderInventoryComponent::SwitchWeaponToInventory(
	const FWeaponSlot NewWeapon, const int32 IndexSlot, const int32 CurrentIndexWeaponChar, FDropItem& DropItemInfo)
{
	bool bResult = false;

	if (WeaponSlots.IsValidIndex(IndexSlot) && GetDropItemInfoFromInventory(IndexSlot, DropItemInfo))
	{
		WeaponSlots[IndexSlot] = NewWeapon;

		SwitchWeaponToIndexByNextPrevIndex(CurrentIndexWeaponChar, -1, NewWeapon.AdditionalInfo, true);

		// OnUpdateWeaponSlots.Broadcast(IndexSlot, NewWeapon);
		UpdateWeaponSlotsEvent_Multicast(IndexSlot, NewWeapon);
		bResult = true;
	}
	return bResult;
}

void UPowderInventoryComponent::TryGetWeaponToInventory_OnServer_Implementation(AActor* PickUpActor, const FWeaponSlot NewWeapon)
{
	int32 IndexSlot = -1;
	if (CheckCanTakeWeapon(IndexSlot))
	{
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			WeaponSlots[IndexSlot] = NewWeapon;

			// OnUpdateWeaponSlots.Broadcast(indexSlot, NewWeapon);
			UpdateWeaponSlotsEvent_Multicast(IndexSlot, NewWeapon);

			if (PickUpActor)
			{
				PickUpActor->Destroy();
			}
		}
	}
}

void UPowderInventoryComponent::DropWeaponByIndex_OnServer_Implementation(const int32 ByIndex)
{
	FDropItem DropItemInfo;

	bool bIsCanDrop = false;
	int8 i = 0;
	int8 AvailableWeaponNum = 0;
	while (i < WeaponSlots.Num() && !bIsCanDrop)
	{
		if (!WeaponSlots[i].NameItem.IsNone())
		{
			AvailableWeaponNum++;
			if (AvailableWeaponNum > 1)
			{
				bIsCanDrop = true;
			}
		}
		i++;
	}

	if (bIsCanDrop && WeaponSlots.IsValidIndex(ByIndex) && GetDropItemInfoFromInventory(ByIndex, DropItemInfo))
	{
		const FWeaponSlot EmptyWeaponSlot;
		int8 j = 0;
		while (j < WeaponSlots.Num())
		{
			if (!WeaponSlots[j].NameItem.IsNone())
			{
				// OnSwitchWeapon.Broadcast(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalInfo, j);
				SwitchWeaponEvent_OnServer(WeaponSlots[j].NameItem, WeaponSlots[j].AdditionalInfo, j);
			}
			j++;
		}

		WeaponSlots[ByIndex] = EmptyWeaponSlot;
		if (GetOwner()->GetClass()->ImplementsInterface(UPowderInteraction::StaticClass()))
		{
			IPowderInteraction::Execute_DropWeaponToWorld(GetOwner(), DropItemInfo);
		}

		// OnUpdateWeaponSlots.Broadcast(ByIndex, EmtyWeaponSlot);
		UpdateWeaponSlotsEvent_Multicast(ByIndex, EmptyWeaponSlot);
	}
}

bool UPowderInventoryComponent::GetDropItemInfoFromInventory(const int32 IndexSlot, FDropItem& DropItemInfo)
{
	bool bResult = false;

	const FName DropItemName = GetWeaponNameBySlotIndex(IndexSlot);

	UPowderGameInstance* MyGameInstance = Cast<UPowderGameInstance>(GetWorld()->GetGameInstance());
	if (MyGameInstance)
	{
		bResult = MyGameInstance->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);
		if (WeaponSlots.IsValidIndex(IndexSlot))
		{
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[IndexSlot].AdditionalInfo;
		}
	}

	return bResult;
}

void UPowderInventoryComponent::InitInventory_OnServer_Implementation(
	const TArray<FWeaponSlot>& NewWeaponSlotsInfo, const TArray<FAmmoSlot>& NewAmmoSlotsInfo)
{
	WeaponSlots = NewWeaponSlotsInfo;
	AmmoSlots = NewAmmoSlotsInfo;
	// Find init weaponsSlots and First Init Weapon

	MaxSlotsWeapon = WeaponSlots.Num();

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
		{
			// OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
			SwitchWeaponEvent_OnServer(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
		}
	}
}

void UPowderInventoryComponent::AmmoChangeEvent_Multicast_Implementation(const EWeaponType TypeWeapon, const int32 Count)
{
	OnAmmoChange.Broadcast(TypeWeapon, Count);
}

void UPowderInventoryComponent::SwitchWeaponEvent_OnServer_Implementation(
	const FName WeaponName, const FAdditionalWeaponInfo AdditionalInfo, const int32 IndexSlot)
{
	OnSwitchWeapon.Broadcast(WeaponName, AdditionalInfo, IndexSlot);
}

void UPowderInventoryComponent::WeaponAdditionalInfoChangeEvent_Multicast_Implementation(
	int32 IndexSlot, FAdditionalWeaponInfo AdditionalInfo)
{
	OnWeaponAdditionalInfoChange.Broadcast(IndexSlot, AdditionalInfo);
}

void UPowderInventoryComponent::WeaponAmmoEmptyEvent_Multicast_Implementation(EWeaponType TypeWeapon)
{
	OnWeaponAmmoEmpty.Broadcast(TypeWeapon);
}

void UPowderInventoryComponent::WeaponAmmoAvailableEvent_Multicast_Implementation(EWeaponType TypeWeapon)
{
	OnWeaponAmmoAvailable.Broadcast(TypeWeapon);
}

void UPowderInventoryComponent::UpdateWeaponSlotsEvent_Multicast_Implementation(int32 IndexSlotChange, FWeaponSlot NewInfo)
{
	OnUpdateWeaponSlots.Broadcast(IndexSlotChange, NewInfo);
}

void UPowderInventoryComponent::WeaponNotHaveRoundEvent_Multicast_Implementation(int32 IndexSlotWeapon)
{
	OnWeaponNotHaveRound.Broadcast(IndexSlotWeapon);
}

void UPowderInventoryComponent::WeaponHaveRoundEvent_Multicast_Implementation(int32 IndexSlotWeapon)
{
	OnWeaponHaveRound.Broadcast(IndexSlotWeapon);
}

TArray<FWeaponSlot> UPowderInventoryComponent::GetWeaponSlots()
{
	return WeaponSlots;
}

TArray<FAmmoSlot> UPowderInventoryComponent::GetAmmoSlots()
{
	return AmmoSlots;
}

void UPowderInventoryComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UPowderInventoryComponent, WeaponSlots);
	DOREPLIFETIME(UPowderInventoryComponent, AmmoSlots);
}
