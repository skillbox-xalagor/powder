// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Character/ALSPlayerController.h"
#include "GameFramework/PlayerController.h"
#include "PowderPlayerController.generated.h"

UCLASS()
class APowderPlayerController : public AALSPlayerController
{
	GENERATED_BODY()
};
