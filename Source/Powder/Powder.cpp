// Copyright Epic Games, Inc. All Rights Reserved.

#include "Powder.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, Powder, "Powder");

DEFINE_LOG_CATEGORY(LogPowder)
DEFINE_LOG_CATEGORY(LogPowder_Net)
