// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Powder : ModuleRules
{
	public Powder(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new[]
		{
			"Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule",
			"PhysicsCore", "Slate", "ALSV4_CPP", "Slate", "SlateCore", "UMG", "CommonInput", "CommonUI",
		});
	}
}