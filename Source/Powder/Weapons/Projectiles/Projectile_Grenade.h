// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Powder/Weapons/Projectiles/Projectile.h"
#include "Projectile_Grenade.generated.h"

/**
 * 
 */
UCLASS()
class POWDER_API AProjectile_Grenade : public AProjectile
{
	GENERATED_BODY()


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);

	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		FVector NormalImpulse, const FHitResult& Hit) override;

	virtual void ImpactProjectile() override;

	UFUNCTION(Server, Reliable)
	void Explose_Onserver();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
	bool TimerEnabled = false;

	float TimerToExplose = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
	float TimeToExplose = 5.0f;

	UFUNCTION(NetMulticast, Reliable)
	void SpawnExplodeFX_Multicast(UParticleSystem* FxTemplate);
	UFUNCTION(NetMulticast, Reliable)
	void SpawnExplodeSound_Multicast(USoundBase* ExplodeSound);
};
