// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "Powder/StateEffects/PowderStateEffect.h"
#include "Powder/FuncLibrary/Types.h"
#include "PowderInteraction.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UPowderInteraction : public UInterface
{
	GENERATED_BODY()
};

/**
 For communicated all game object in world 
 */
class POWDER_API IPowderInteraction
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual EPhysicalSurface GetSurfaceType();
	virtual TArray<UPowderStateEffect*> GetAllCurrentEffects();
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void RemoveEffect(UPowderStateEffect* RemoveEffect);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void AddEffect(UPowderStateEffect* newEffect);

	//inv
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropWeaponToWorld(FDropItem DropItemInfo);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent)
	void DropAmmoToWorld(EWeaponType TypeAmmo, int32 Cout);
};
