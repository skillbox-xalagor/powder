// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogPowder, Log, All);

DECLARE_LOG_CATEGORY_EXTERN(LogPowder_Net, Log, All);
