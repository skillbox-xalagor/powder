// Fill out your copyright notice in the Description page of Project Settings.


#include "PowderStateEffect.h"
#include "Powder/Character/PowderHealthComponent.h"
#include "Powder/Interface/PowderInteraction.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

bool UPowderStateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{
	myActor = Actor;
	NameBone = NameBoneHit;

	IPowderInteraction* myInterface = Cast<IPowderInteraction>(myActor);
	if (myInterface)
	{
		myInterface->Execute_AddEffect(myActor, this);
	}

	return true;
}

void UPowderStateEffect::DestroyObject()
{
	IPowderInteraction* myInterface = Cast<IPowderInteraction>(myActor);
	if (myInterface)
	{
		myInterface->Execute_RemoveEffect(myActor, this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}

bool UPowderStateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();
	return true;
}

void UPowderStateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UPowderStateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UPowderHealthComponent* myHealthComp = Cast<UPowderHealthComponent>(
			myActor->GetComponentByClass(UPowderHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);
		}
	}

	DestroyObject();
}

bool UPowderStateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UPowderStateEffect_ExecuteTimer::DestroyObject,
			Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UPowderStateEffect_ExecuteTimer::Execute, RateTime,
			true);
	}

	//if (ParticleEffect)
	//{
	//FXSpawnByStateEffect_Multicast(ParticleEffect, NameBoneHit);		To REmove
	//}

	return true;
}

void UPowderStateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
	}

	//ParticleEmitter->DestroyComponent();
	//ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UPowderStateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UPowderHealthComponent* myHealthComp = Cast<UPowderHealthComponent>(
			myActor->GetComponentByClass(UPowderHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue_OnServer(Power);
		}
	}
}

void UPowderStateEffect::FXSpawnByStateEffect_Multicast_Implementation(UParticleSystem* Effect, FName NameBoneHit)
{
	//ToDo for object with interface create func return offset, Name Bones, 
	//ToDo Random init Effect with available array (For)
	/*if (Effect)
	{
		FName NameBoneToAttached = NameBoneHit;
		FVector Loc = FVector(0);


		USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		if (myMesh)
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(Effect, myMesh, NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
		else
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(Effect, myActor->GetRootComponent(), NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
		}
	}*/
}

void UPowderStateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UPowderStateEffect, NameBone);
}
