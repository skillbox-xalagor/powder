#pragma once

#include "CommonButtonBase.h"

#include "PowderButton.generated.h"


UCLASS(Abstract, meta = (DisableNativeTick))
class UPowderButton : public UCommonButtonBase
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void SetPowderText(const FText& InText);

protected:
	virtual void NativeOnCurrentTextStyleChanged() override;

	UPROPERTY(meta = (BindWidget))
	class UCommonTextBlock* MyButtonTextLabel;
};