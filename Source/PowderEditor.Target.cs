// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class PowderEditorTarget : TargetRules
{
	public PowderEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("Powder");
		DefaultBuildSettings = BuildSettingsVersion.V2;
	}
}