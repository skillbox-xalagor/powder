// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "PowderStateEffect.generated.h"

/**
 All implementation effects in one file, Fire Effect, blooding and...
 */
UCLASS(Blueprintable, BlueprintType)
class POWDER_API UPowderStateEffect : public UObject
{
	GENERATED_BODY()
public:
	virtual bool IsSupportedForNetworking() const override
	{
		return true;
	};
	virtual bool InitObject(AActor* Actor, FName NameBoneHit);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	bool bIsStakable = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	UParticleSystem* ParticleEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	bool bIsAutoDestroyParticleEffect = false;

	AActor* myActor = nullptr;
	UPROPERTY(Replicated)
	FName NameBone;

	UFUNCTION(NetMulticast, Reliable)
	void FXSpawnByStateEffect_Multicast(UParticleSystem* Effect, FName NameBoneHit);
};

UCLASS()
class POWDER_API UPowderStateEffect_ExecuteOnce : public UPowderStateEffect
{
	GENERATED_BODY()

public:
	virtual bool InitObject(AActor* Actor, FName NameBoneHit) override;
	virtual void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
	float Power = 20.0f;
};

UCLASS()
class POWDER_API UPowderStateEffect_ExecuteTimer : public UPowderStateEffect
{
	GENERATED_BODY()

public:
	virtual bool InitObject(AActor* Actor, FName NameBoneHit) override;

	virtual void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
	float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
};
