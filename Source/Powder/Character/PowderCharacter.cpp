// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "PowderCharacter.h"

#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"
#include "Engine/ActorChannel.h"
#include "Engine/World.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"
#include "Particles/ParticleSystemComponent.h"
#include "Powder/Game/PowderGameInstance.h"
#include "Powder/Powder.h"
#include "Powder/Weapons/Projectiles/Projectile.h"
#include "DrawDebugHelpers.h"

APowderCharacter::APowderCharacter(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	MyInventoryComponent = CreateDefaultSubobject<UPowderInventoryComponent>(TEXT("MyInventoryComponent"));
	MyHealthComponent = CreateDefaultSubobject<UPowderCharacterHealthComponent>(TEXT("HealthComponent"));

	if (MyHealthComponent)
	{
		MyHealthComponent->OnDead.AddDynamic(this, &APowderCharacter::CharDead);
	}
	if (MyInventoryComponent)
	{
		MyInventoryComponent->OnSwitchWeapon.AddDynamic(this, &APowderCharacter::InitWeapon);
	}
}

void APowderCharacter::Tick(const float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	MovementTick(DeltaSeconds);
}

void APowderCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction(TEXT("FireAction"), IE_Pressed, this, &APowderCharacter::InputAttackPressed);
	PlayerInputComponent->BindAction(TEXT("FireAction"), IE_Released, this, &APowderCharacter::InputAttackReleased);
	PlayerInputComponent->BindAction(TEXT("ReloadAction"), IE_Released, this, &APowderCharacter::TryReloadWeapon);
	PlayerInputComponent->BindAction(TEXT("NextWeaponAction"), IE_Pressed, this, &APowderCharacter::TrySwitchNextWeapon);
	PlayerInputComponent->BindAction(TEXT("PrevWeaponAction"), IE_Pressed, this, &APowderCharacter::TrySwitchPrevWeapon);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	PlayerInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &APowderCharacter::TKeyPressed<1>);
	PlayerInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &APowderCharacter::TKeyPressed<2>);
	PlayerInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &APowderCharacter::TKeyPressed<3>);
	PlayerInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &APowderCharacter::TKeyPressed<4>);
	PlayerInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &APowderCharacter::TKeyPressed<5>);
	PlayerInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &APowderCharacter::TKeyPressed<6>);
	PlayerInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &APowderCharacter::TKeyPressed<7>);
	PlayerInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &APowderCharacter::TKeyPressed<8>);
	PlayerInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &APowderCharacter::TKeyPressed<9>);
	PlayerInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &APowderCharacter::TKeyPressed<0>);
}

void APowderCharacter::InputAttackPressed()
{
	if (MyHealthComponent && MyHealthComponent->GetIsAlive())
	{
		AttackCharEvent(true);
	}
}

void APowderCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void APowderCharacter::MovementTick(float DeltaTime)
{
	if (MyHealthComponent && MyHealthComponent->GetIsAlive())
	{
		if (GetController() && GetController()->IsLocalPlayerController())
		{
			APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			FVector FinalAim = FVector::ZeroVector;
			if (MyController)
			{
				FCollisionQueryParams RV_TraceParams = FCollisionQueryParams(FName(TEXT("RV_Trace")), true, this);
				RV_TraceParams.bTraceComplex = true;
				RV_TraceParams.bReturnPhysicalMaterial = false;

				// Re-initialize hit info
				FHitResult ResultHit(ForceInit);

				FVector CamLoc;
				FRotator CamRot;
				MyController->GetPlayerViewPoint(CamLoc, CamRot);
				FinalAim = CamRot.Vector();

				// call GetWorld() from within an actor extending class
				GetWorld()->LineTraceSingleByChannel(ResultHit, FinalAim, FinalAim * 20000, ECC_Visibility, RV_TraceParams);

				const float FindRotatorResultYaw =
					UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;

				if (CurrentWeapon)
				{
					FVector Displacement = FVector(0);
					constexpr bool bIsReduceDispersion = false;
					switch (MovementState)
					{
						case EALSGait::Walking:
							Displacement = FVector(0.0f, 0.0f, 0.0f);
							// CurrentWeapon->ShouldReduceDispersion = false;
							break;
						case EALSGait::Running:
							Displacement = FVector(0.0f, 0.0f, 0.0f);
							// CurrentWeapon->ShouldReduceDispersion = false;
							break;
						case EALSGait::Sprinting:
							break;
						default:
							break;
					}

					// CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
					CurrentWeapon->UpdateWeaponByCharacterMovementState_OnServer(
						ResultHit.Location + Displacement, bIsReduceDispersion);
					// aim cursor like 3d Widget?
				}
			}
		}
	}
}

bool APowderCharacter::GetIsAlive() const
{
	bool bResult = false;
	if (MyHealthComponent)
	{
		bResult = MyHealthComponent->GetIsAlive();
	}
	return bResult;
}

void APowderCharacter::AttackCharEvent(const bool bIsFiring) const
{
	AWeapon* MyWeapon = GetCurrentWeapon();
	if (MyWeapon)
	{
		MyWeapon->SetWeaponStateFire_OnServer(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("APowderCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

AWeapon* APowderCharacter::GetCurrentWeapon() const
{
	return CurrentWeapon;
}

void APowderCharacter::InitWeapon(
	const FName IdWeaponName, const FAdditionalWeaponInfo WeaponAdditionalInfo, const int32 NewCurrentIndexWeapon)
{
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::White, (TEXT("Weapon Init: %s"), IdWeaponName.ToString()));
	// Go on server
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UPowderGameInstance* MyGameInstance = Cast<UPowderGameInstance>(GetGameInstance());
	if (MyGameInstance)
	{
		FWeaponInfo MyWeaponInfo;
		if (MyGameInstance->GetWeaponInfoByName(IdWeaponName, MyWeaponInfo))
		{
			if (MyWeaponInfo.WeaponClass)
			{
				const FVector SpawnLocation = FVector(0);
				const FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeapon* MyWeapon =
					Cast<AWeapon>(GetWorld()->SpawnActor(MyWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (MyWeapon)
				{
					const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					MyWeapon->AttachToComponent(GetMesh(), Rule, TEXT("VB RHS_ik_hand_gun"));
					CurrentWeapon = MyWeapon;

					MyWeapon->IdWeaponName = IdWeaponName;
					MyWeapon->WeaponSetting = MyWeaponInfo;

					MyWeapon->ReloadTime = MyWeaponInfo.ReloadTime;
					MyWeapon->UpdateStateWeapon_OnServer(GetGait());

					MyWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;

					CurrentIndexWeapon = NewCurrentIndexWeapon;

					MyWeapon->OnWeaponReloadStart.AddDynamic(this, &APowderCharacter::WeaponReloadStart);
					MyWeapon->OnWeaponReloadEnd.AddDynamic(this, &APowderCharacter::WeaponReloadEnd);

					MyWeapon->OnWeaponFireStart.AddDynamic(this, &APowderCharacter::WeaponFireStart);

					// after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
					{
						CurrentWeapon->InitReload();
					}

					if (MyInventoryComponent)
					{
						MyInventoryComponent->OnWeaponAmmoAvailable.Broadcast(MyWeapon->WeaponSetting.WeaponType);
					}

					SetOverlayState(CurrentWeapon->WeaponSetting.OverlayState, true);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("APowderCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}

void APowderCharacter::TryReloadWeapon()
{
	if (MyHealthComponent && MyHealthComponent->GetIsAlive() && CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		TryReloadWeapon_OnServer();
	}
}

void APowderCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void APowderCharacter::WeaponReloadEnd(const bool bIsSuccess, const int32 AmmoSafe)
{
	if (MyInventoryComponent && CurrentWeapon)
	{
		MyInventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoSafe);
		MyInventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void APowderCharacter::TrySwitchWeaponToIndexByKeyInput_OnServer_Implementation(const int32 ToIndex)
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && MyInventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && MyInventoryComponent)
		{
			const int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->WeaponReloading)
				{
					CurrentWeapon->CancelReload();
				}
			}

			MyInventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
}

void APowderCharacter::DropCurrentWeapon() const
{
	if (MyInventoryComponent)
	{
		MyInventoryComponent->DropWeaponByIndex_OnServer(CurrentIndexWeapon);
	}
}

void APowderCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void APowderCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}

void APowderCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (MyInventoryComponent && CurrentWeapon)
	{
		MyInventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponFireStart_BP(Anim);
}

void APowderCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void APowderCharacter::TrySwitchNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && MyInventoryComponent->WeaponSlots.Num() > 1)
	{
		// We have more then one weapon go switch
		const int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (MyInventoryComponent)
		{
			MyInventoryComponent->SwitchWeaponToIndexByNextPrevIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true);
		}
	}
}

void APowderCharacter::TrySwitchPrevWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && MyInventoryComponent->WeaponSlots.Num() > 1)
	{
		// We have more then one weapon go switch
		const int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
			}
		}

		if (MyInventoryComponent)
		{
			// MyInventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (MyInventoryComponent->SwitchWeaponToIndexByNextPrevIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}

EPhysicalSurface APowderCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = SurfaceType_Default;
	if (MyHealthComponent)
	{
		if (MyHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* MyMaterial = GetMesh()->GetMaterial(0);
				if (MyMaterial)
				{
					Result = MyMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return Result;
}

TArray<UPowderStateEffect*> APowderCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void APowderCharacter::RemoveEffect_Implementation(UPowderStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);

	if (!RemoveEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(RemoveEffect, false);
		EffectRemove = RemoveEffect;
	}
}

void APowderCharacter::AddEffect_Implementation(UPowderStateEffect* NewEffect)
{
	Effects.Add(NewEffect);

	if (!NewEffect->bIsAutoDestroyParticleEffect)
	{
		SwitchEffect(NewEffect, true);
		EffectAdd = NewEffect;
	}
	else
	{
		if (NewEffect->ParticleEffect)
		{
			ExecuteEffectAdded_OnServer(NewEffect->ParticleEffect);
		}
	}
}

void APowderCharacter::CharDead_BP_Implementation()
{
	// BP
}

void APowderCharacter::TryReloadWeapon_OnServer_Implementation()
{
	if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
	{
		CurrentWeapon->InitReload();
	}
}

void APowderCharacter::EffectAdd_OnRep()
{
	if (EffectAdd)
	{
		SwitchEffect(EffectAdd, true);
	}
}

void APowderCharacter::EffectRemove_OnRep()
{
	if (EffectRemove)
	{
		SwitchEffect(EffectRemove, false);
	}
}

void APowderCharacter::ExecuteEffectAdded_OnServer_Implementation(UParticleSystem* ExecuteFX)
{
	ExecuteEffectAdded_Multicast(ExecuteFX);
}

void APowderCharacter::ExecuteEffectAdded_Multicast_Implementation(UParticleSystem* ExecuteFX)
{
	UTypes::ExecuteEffectAdded(ExecuteFX, this, FVector(0), FName("Spine_01"));
}

void APowderCharacter::SwitchEffect(UPowderStateEffect* Effect, const bool bIsAdd)
{
	if (bIsAdd)
	{
		if (Effect && Effect->ParticleEffect)
		{
			const FName NameBoneToAttached = Effect->NameBone;
			const FVector Loc = FVector(0);

			USkeletalMeshComponent* MyMesh = GetMesh();
			if (MyMesh)
			{
				UParticleSystemComponent* NewParticleSystem = UGameplayStatics::SpawnEmitterAttached(Effect->ParticleEffect, MyMesh,
					NameBoneToAttached, Loc, FRotator::ZeroRotator, EAttachLocation::SnapToTarget, false);
				ParticleSystemEffects.Add(NewParticleSystem);
			}
		}
	}
	else
	{
		if (Effect && Effect->ParticleEffect)
		{
			if (ParticleSystemEffects.Num() > 0)
			{
				bool bIsFind = false;
				int32 i = 0;
				while (i < ParticleSystemEffects.Num() && !bIsFind)
				{
					if (ParticleSystemEffects[i] && ParticleSystemEffects[i]->Template && Effect->ParticleEffect &&
						Effect->ParticleEffect == ParticleSystemEffects[i]->Template)
					{
						bIsFind = true;
						ParticleSystemEffects[i]->DeactivateSystem();
						ParticleSystemEffects[i]->DestroyComponent();
						ParticleSystemEffects.RemoveAt(i);
					}
					i++;
				}
			}
		}
	}
}

void APowderCharacter::CharDead()
{
	CharDead_BP();
	if (!HasAuthority())
		AttackCharEvent(false);
	else
	{
		float TimeAnim = 0.0f;
		const int32 Rnd = FMath::RandHelper(DeathsAnim.Num());
		if (DeathsAnim.IsValidIndex(Rnd) && DeathsAnim[Rnd] && GetMesh() && GetMesh()->GetAnimInstance())
		{
			Replicated_PlayMontage(DeathsAnim[Rnd], 1.f);
		}
		else
		{
			ReplicatedRagdollStart();
		}

		if (GetController())
		{
			GetController()->UnPossess();
		}

		const float DecreaseAnimTimer = FMath::FRandRange(0.2f, 1.0f);

		GetWorldTimerManager().SetTimer(
			TimerHandle_RagDollTimer, this, &AALSBaseCharacter::ReplicatedRagdollStart, TimeAnim - DecreaseAnimTimer, false);

		SetLifeSpan(20.0f);
		if (GetCurrentWeapon())
		{
			GetCurrentWeapon()->SetLifeSpan(20.0f);
		}
	}
}

float APowderCharacter::TakeDamage(
	const float DamageAmount, const FDamageEvent& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	const float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (MyHealthComponent && MyHealthComponent->GetIsAlive())
	{
		MyHealthComponent->ChangeHealthValue_OnServer(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectile* MyProjectile = Cast<AProjectile>(DamageCauser);
		if (MyProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, NAME_None, MyProjectile->ProjectileSetting.Effect, GetSurfaceType());
			// to do Name_None - bone for radial damage
		}
	}

	return ActualDamage;
}

bool APowderCharacter::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool Wrote = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);

	for (int32 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i])
		{
			Wrote |= Channel->ReplicateSubobject(Effects[i], *Bunch, *RepFlags);
		}
	}
	return Wrote;
}

TArray<UPowderStateEffect*> APowderCharacter::GetCurrentEffectsOnChar()
{
	return Effects;
}

void APowderCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APowderCharacter, CurrentWeapon);
	DOREPLIFETIME(APowderCharacter, CurrentIndexWeapon);
	DOREPLIFETIME(APowderCharacter, Effects);
	DOREPLIFETIME(APowderCharacter, EffectAdd);
	DOREPLIFETIME(APowderCharacter, EffectRemove);
}
